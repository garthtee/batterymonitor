package com.example.l00113337.batterymonitor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by GarthToland on 25/06/2015.
 */
public class MyDBHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 10;
    private static final String DATABASE_NAME = "batteryReadingDB.db";
    private static final String TABLE_BATTERYREADING = "batteryreading";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_TIMESTAMP = "timestamp";
    private static final String COLUMN_TIMERSTART = "timerstart";
    private static final String COLUMN_STARTLEVEL = "startlevel";
    private static final String COLUMN_FINISHLEVEL = "finishlevel";
    private ArrayList<Reading> readings;


    // Passing database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE " + TABLE_BATTERYREADING + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP, "
                + COLUMN_TIMERSTART + " TEXT, "
                + COLUMN_STARTLEVEL + " TEXT, "
                + COLUMN_FINISHLEVEL + " TEXT "
                + ");";
        sqLiteDatabase.execSQL(query);

    }

    public void addReading(Reading reading) {

        // placing values received into contentValues variable
        ContentValues values = new ContentValues();
        values.put(COLUMN_TIMERSTART, reading.getTimeStart());
        values.put(COLUMN_STARTLEVEL, reading.getStartLevel());
        values.put(COLUMN_FINISHLEVEL, reading.getFinishLevel());

        // placing all values in database table batteryreading
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_BATTERYREADING, null, values);

        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BATTERYREADING);
        onCreate(sqLiteDatabase);

    }

    public ArrayList<Reading> databaseToString() {

        int dbID = 0;
        String dbTimerStart = "";
        String dbStartLevel = "";
        String dbFinishLevel = "";
        readings = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_BATTERYREADING + " WHERE 1";
        //Cursor points to a location in your results
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        //Move to the first row in your results
        cursor.moveToFirst();

        //Position after the last row means the end of the results
        while (!cursor.isAfterLast()) {

            if (cursor.getInt(cursor.getColumnIndex("_id")) != 0) {
                dbID = cursor.getInt(cursor.getColumnIndex("_id"));
            }
            //if (cursor.getString(cursor.getColumnIndex("timestamp")) != null) {
              //  dbTimeStamp = cursor.getString(cursor.getColumnIndex("timestamp"));
            //}
            if (cursor.getString(cursor.getColumnIndex("timerstart")) != null) {
                dbTimerStart = cursor.getString(cursor.getColumnIndex("timerstart"));
            }
            if (cursor.getString(cursor.getColumnIndex("startlevel")) != null) {
                dbStartLevel = cursor.getString(cursor.getColumnIndex("startlevel"));
            }
            if (cursor.getString(cursor.getColumnIndex("finishlevel")) != null) {
                dbFinishLevel = cursor.getString(cursor.getColumnIndex("finishlevel"));
            }

            readings.add(new Reading(dbID, dbTimerStart, dbStartLevel, dbFinishLevel));
            cursor.moveToNext();

        }

        sqLiteDatabase.close();
        return readings;
    }
}