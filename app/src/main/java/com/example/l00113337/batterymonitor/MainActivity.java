package com.example.l00113337.batterymonitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.BatteryManager;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.util.ArrayList;


public class MainActivity extends Activity {

    private TextView batteryCurrentText, txtCurrentPercentageBattery, txtTimerLength, txtTimerCurrent, txtLevelStart;
    private int  voltage, batteryPercentageRemaining, startLevel, finishLevel, totalPercentageConsumed;
    private ToneGenerator toneG;
    private MyDBHandler dbHandler;
    private long countdownLength;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //****************** Assign views by ID *****************************
        txtCurrentPercentageBattery = (TextView) findViewById(R.id.txtCurrentPercentageBattery);
        txtTimerLength = (TextView) findViewById(R.id.txtTimerLength);
        txtTimerCurrent = (TextView) findViewById(R.id.txtTimerCurrent);
        txtLevelStart = (TextView) findViewById(R.id.txtLevelStart);

        // Creating an instance of MyDBHandler
        dbHandler = new MyDBHandler(this, null, null, 1);

        // Tone generator
        toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 80); // 100 is the volume of sound 100 being loud!


        // connecting receiver method with on create method
        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    public void onClick_TimerStart(View view)
    {

        ///////////////////////////////////////////////////////////////
        //                Pop-up time input dialog                   //
        ///////////////////////////////////////////////////////////////

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Set timer..\n(Enter in minutes)");

        // Construct up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME | InputType.TYPE_CLASS_DATETIME);
        dialogBuilder.setView(input);

        // Set up the buttons
        dialogBuilder.setPositiveButton("Start", new DialogInterface.OnClickListener() { // Set button onClick method
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //Toast.makeText(getApplicationContext(), "" , Toast.LENGTH_LONG).show();

                startLevel = batteryPercentageRemaining; // Storing the current battery percentage
                txtLevelStart.setText("" + startLevel + "%");

                countdownLength = Long.parseLong(input.getText().toString());
                countdownLength = countdownLength * 60000; // multiplying by 60000 as to convert variable to milliseconds as it's in minutes

                txtTimerLength.setText("" +(countdownLength / 60000) +" Minutes");

                new CountDownTimer(countdownLength, 1000) {

                    public void onTick(long millisUntilFinished) {

                        int seconds = (int) (millisUntilFinished / 1000) % 60;
                        int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                        int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);

                        txtTimerCurrent.setText(hours + " : " + minutes + " : " + seconds);
                    }

                    public void onFinish() {


                        finishLevel = batteryPercentageRemaining;

                        int totalPercentageConsumed = finishLevel - startLevel;



                        txtTimerCurrent.setText("Complete!");
                        toneG.startTone(ToneGenerator.TONE_CDMA_MED_SSL, 4000); // Plays a tone to for 4 seconds(4000 milliseconds) when timer is complete

                        int i = 0;
                        String s = null;
                        // Converting all values to Strings as to store in database
                        String startLevelString = String.valueOf(startLevel);
                        String finishLevelString = String.valueOf(finishLevel);
                        String timeStartString = String.valueOf(countdownLength / 60000);
                        // Adding to database
                        dbHandler.addReading(new Reading(i, timeStartString, startLevelString, finishLevelString));
                        Toast.makeText(getApplicationContext(), "Saved to database!", Toast.LENGTH_SHORT).show();



                    }
                }.start();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {   // Cancel button onClick method
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialogBuilder.show();
    }

    public void onClick_Results(View view){

        Intent myIntent = new Intent(MainActivity.this, Results.class);
        startActivity(myIntent);
    }

    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            BatteryManager mBatteryManager = (BatteryManager) getApplicationContext().getSystemService(Context.BATTERY_SERVICE);

            batteryPercentageRemaining = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);


            txtCurrentPercentageBattery.setText("" +batteryPercentageRemaining +"%");
        }
    };


}



///////////////    Calculations    ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
//finishNWHrs = (totalNWHrs * (batteryPercentageRemaining / 100)); // calculate percentage

