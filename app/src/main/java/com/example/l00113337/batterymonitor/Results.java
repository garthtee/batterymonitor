package com.example.l00113337.batterymonitor;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Results extends ActionBarActivity {

    private Runnable viewReadings;
    private MyDBHandler dbHandler;
    private ArrayList<Reading> mResults;
    private readingsAdapter mAdapter;
    private ListView listView;
    private ProgressDialog mProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        // Creating an instance of MyDBHandler
        dbHandler = new MyDBHandler(this, null, null, 1);

        mResults = new ArrayList<Reading>();
        dbHandler = new MyDBHandler(this, null, null, 1);
        this.mAdapter = new readingsAdapter(this, R.layout.custom_row, mResults);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(this.mAdapter);


        viewReadings = new Runnable(){
            @Override
            public void run() {
                getReadings();
            }
        };
        Thread thread =  new Thread(null, viewReadings, "MagentoBackground");
        thread.start();

        mProgressDialog = ProgressDialog.show(Results.this,
                "Please wait...", "Gathering information ...", true);

    }

    private Runnable returnRes = new Runnable() {

        @Override
        public void run() {
            if(mResults != null && mResults.size() > 0){

                mAdapter.notifyDataSetChanged();

                for(int i=0; i < mResults.size(); i++)
                    mAdapter.add(mResults.get(i));
            }
            mProgressDialog.dismiss();
            mAdapter.notifyDataSetChanged();
        }
    };
    private void getReadings(){
        try{
            mResults = new ArrayList<>();
            mResults = dbHandler.databaseToString();
            Thread.sleep(2000);
        }
        catch (Exception e) {
            Log.e("BACKGROUND_PROC", e.getMessage());
        }
        runOnUiThread(returnRes);
    }

    private class readingsAdapter extends ArrayAdapter<Reading> {

        private ArrayList<Reading> readings;

        public readingsAdapter(Context context, int textViewResourceId, ArrayList<Reading> readings) {
            super(context, textViewResourceId, readings);
            this.readings = readings;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.custom_row, null);
            }
            Reading reading = readings.get(position);
            if (reading != null) {
                TextView tt2 = (TextView) v.findViewById(R.id.txtID);
                if (tt2 != null) {
                    tt2.setText("Test " +reading.getID());
                }
                TextView tt3 = (TextView) v.findViewById(R.id.txtTimerStart);
                if (tt3 != null) {
                    tt3.setText("Time = " +reading.getTimeStart() +" Minutes");
                }
                TextView tt4 = (TextView) v.findViewById(R.id.txtStartLevel);
                if (tt4 != null) {
                    tt4.setText("Start Level = " +reading.getStartLevel() +"%");
                }
                TextView tt5 = (TextView) v.findViewById(R.id.txtFinishLevel);
                if (tt5 != null) {
                    tt5.setText("Finish Level = " +reading.getFinishLevel() +"%");
                }
            }
            return v;
        }
    }


}
