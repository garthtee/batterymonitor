package com.example.l00113337.batterymonitor;

/**
 * Created by GarthToland on 25/06/2015.
 */
public class Reading {

    private int id;
    private String timeStart;
    private String startLevel;
    private String finishLevel;

    public Reading(int id, String timeStart, String startLevel, String finishLevel) {

        this.id = id;
        this.timeStart = timeStart;
        this.startLevel = startLevel;
        this.finishLevel = finishLevel;

    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getStartLevel() {
        return startLevel;
    }

    public void setStartLevel(String startLevel) {
        this.startLevel = startLevel;
    }

    public String getFinishLevel() {
        return finishLevel;
    }

    public void setFinishLevel(String finishLevel) {
        this.finishLevel = finishLevel;
    }
}
